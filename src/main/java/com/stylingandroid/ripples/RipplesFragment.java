package com.stylingandroid.ripples;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class RipplesFragment extends Fragment {
    public static final String ARG_LAYOUT = "com.stylingandroid.ripples.ARG_LAYOUT";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int layoutId = getArguments().getInt(ARG_LAYOUT, 0);
        return inflater.inflate(layoutId, container, false);
    }
}
